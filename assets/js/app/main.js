/* DOCUMENT INFORMATION
 - Document: Theme_Name
 - Version:  1.0.0
 - Client:   Client_Name
 - Author:   Emin Azeroglu
 */

$ ( function () {

    /* Only Number */
    $('body').on('input', '.only-number', function () {
        this.value = this.value.replace(/[^0-9]/g, '');
    });

    /* Input Mask */
    $(document).ready(function () {
        $('[data-mask]').inputmask({'mask': '\\9\\94 99 999 99 99'});
    });

    $('.datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
    });

    $('.select2').select2();

    $('.scrollbar-macosx').scrollbar();


    // Showcase slider
    $('.showslider').flexslider({
        animation: "slide",
        minItems: 1,
        maxItems: 3,
        itemWidth: 765,
      });
    
    // News Slider

    $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 325,
        itemMargin: 36,
        minItems: 1,
        maxItems: 6,
    });

    

    // Image gallery

    $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 107,
        itemMargin: 3,
        asNavFor: '#slider'
    });
    
    $('#slider').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
    });



    // Writer Slider

    $('.writerslider').flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 148,
        itemMargin: 0,
        minItems: 1,
        maxItems: 5,
    });

});